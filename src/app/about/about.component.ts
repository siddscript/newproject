import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  @Input() description;

  public itemid = localStorage.getItem('itemid');
  public albumarr = JSON.parse(localStorage.getItem("almarr"))
 
   public selectedalbum=[];
  public trgid;
  name={};
 
  constructor(private route:Router,private location:Location) {  }

  ngOnInit() {
   
    var len = this.albumarr.length;
    for(var i=0;i<len;i++){
      this.trgid = this.albumarr[i].userId;
      if(this.trgid==this.itemid){
          
          this.selectedalbum.push({
            'albumname':this.albumarr[i].title,
            'albumid':this.albumarr[i].id
          });
      }
    }
  }


  showimages(id){
    localStorage.setItem('almid',id);
    this.route.navigateByUrl('imageview');
  }
  backtoprevious(){
    this.location.back();
  }
}
