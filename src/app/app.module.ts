import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule  } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';
import { ShowdataComponent } from './showdata/showdata.component';
import { HoverproductDirective } from './hoverproduct.directive';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule, MatInputModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material';
import {MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { ImageviewComponent } from './imageview/imageview.component';


@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ShowdataComponent,
    HoverproductDirective,
    ImageviewComponent

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([   
      {
        path:'',
        component:ShowdataComponent
      },
      {
        path:'about',
        component:AboutComponent
      },
      
      {
        path:'product',
        component:ShowdataComponent
      },
     
      {
        path:'imageview',
        component:ImageviewComponent
      }
      
     
  ]),

  HttpClientModule,
  BrowserAnimationsModule,
  ReactiveFormsModule,
  FormsModule,
  MatAutocompleteModule,
  MatInputModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
