import { Component, OnInit } from '@angular/core';
import { RemoteserveService } from '../remoteserve.service';
import { Router } from '@angular/router';
import { AboutComponent } from '../about/about.component';
import {HttpClient} from '@angular/common/http';
import { setInjectImplementation } from '@angular/core/src/di/injector_compatibility';


@Component({
  selector: 'app-showdata',
  templateUrl: './showdata.component.html',
  styleUrls: ['./showdata.component.css']
})
export class ShowdataComponent implements OnInit {
  name = {};
  pricecost;
  public productarr = [];
  public albumarr = [];
  public imgarr = [];
  public quntitynum;
  public idnum;
  public trgid;
  bigdes = this.productarr;
  constructor(private prolist:RemoteserveService, private route:Router,private valuenum:HttpClient) { }

  ngOnInit() {
    
    this.prolist.getalbums().subscribe(data=>this.albumarr=data);
    this.prolist.getImages().subscribe(data=>this.imgarr=data);
    this.prolist.getusers().subscribe(data=>this.productarr=data);
  }
  showIndex(pageName:string,id){
    this.idnum = id; 
  
    localStorage.setItem('itemid',id)
    localStorage.setItem("almarr", JSON.stringify(this.albumarr));
    localStorage.setItem("alimgarmarr", JSON.stringify(this.imgarr));    
    this.route.navigateByUrl('about');
    console.log(this.imgarr)
  }
 


}
