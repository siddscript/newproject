import { TestBed } from '@angular/core/testing';

import { RemoteserveService } from './remoteserve.service';

describe('RemoteserveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RemoteserveService = TestBed.get(RemoteserveService);
    expect(service).toBeTruthy();
  });
});
