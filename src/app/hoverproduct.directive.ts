import { Directive, ElementRef, Renderer2, OnInit, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appHoverproduct]'
})
export class HoverproductDirective implements OnInit {
@HostBinding('style.color') color:string = '#999898'; 
  constructor(private elmRef:ElementRef,elmren:Renderer2) { }



ngOnInit(){

}

@HostListener('mouseenter') mouseover(eventData:Event){
  this.color='#dc56e8';
}
@HostListener('mouseleave') mouseleave(eventData:Event){
  this.color="#999898";
}

@HostListener('mouseclick') mouseclick(eventData:Event){
  this.color = "red";
}

}