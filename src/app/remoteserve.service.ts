import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class RemoteserveService {
  private _url = "https://jsonplaceholder.typicode.com/users/";
  private _albums = "https://jsonplaceholder.typicode.com/albums/";
  private _images = "https://jsonplaceholder.typicode.com/photos/";

  constructor(private http:HttpClient) { }

  getusers():Observable<any>{
    return this.http.get<any>(this._url)
  }

  getalbums():Observable<any>{
    return this.http.get<any>(this._albums)
  }
  getImages():Observable<any>{
    return this.http.get<any>(this._images)
  }
}
