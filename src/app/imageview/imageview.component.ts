import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-imageview',
  templateUrl: './imageview.component.html',
  styleUrls: ['./imageview.component.css']
})
export class ImageviewComponent implements OnInit {
public trgalmid =  localStorage.getItem('almid');
public imgarr = JSON.parse(localStorage.getItem("alimgarmarr"))
public selectedImg = [];
public trgid;
name={};
  constructor(private route:Router,private location:Location) { }

  ngOnInit() {
    var len = this.imgarr.length;
    for(var i=0;i<len;i++){
      
      this.trgid = this.imgarr[i].albumId;
      if(this.trgid==this.trgalmid){
          this.selectedImg.push({
            'imgname':this.imgarr[i].title,
            'imgurl':this.imgarr[i].url,
            'imgthumb':this.imgarr[i].thumbnailUrl,
            'imgid':this.imgarr[i].id
          });
      }
    }
  }
  backtoprevious(){
    this.location.back();
  }

}
